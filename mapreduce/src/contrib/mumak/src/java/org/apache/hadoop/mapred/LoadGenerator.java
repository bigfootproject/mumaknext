package org.apache.hadoop.mapred;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.math.MathException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.tools.rumen.DefaultOutputter;
import org.apache.hadoop.tools.rumen.LoggedJob;
import org.apache.hadoop.tools.rumen.Outputter;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class LoadGenerator {

	private static String inputFile;

	/**
	 * @param args
	 * @throws JobBuilderException
	 * @throws ParseException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws MathException 
	 */
	public static void main(String[] args) throws JobBuilderException, IOException, ParseException, InstantiationException, IllegalAccessException, MathException {
		parseArguments(args);
		InputStream input = new FileInputStream(new File(inputFile));
		List<SimulatorJob> jobList = new LinkedList<SimulatorJob>();
		Constructor constructor = new Constructor(SimulatorJob.class);//Car.class is root
		Yaml yaml = new Yaml(constructor);
		
		Outputter<LoggedJob> traceWriter;
		Class<? extends Outputter> clazzTraceOutputter = DefaultOutputter.class;
	    traceWriter = clazzTraceOutputter.newInstance();
	    traceWriter.init(new Path("/home/antonio/traceOutput/output"), new Configuration());
		clazzTraceOutputter.newInstance();
		
		int counter = 0;
		
		for (Object data : yaml.loadAll(input)) {
			SimulatorJob job = (SimulatorJob)data;
			LoggedJob ljob = job.build(counter);
	        counter++;
	        traceWriter.output(ljob);
	        jobList.add(job);
	        System.out.println(job);
	        System.out.println();
	    }
		System.out.println("Read " + counter + " jobs!");
		
		traceWriter.close();
	}
	
	protected static void parseArguments(String[] args) throws ParseException {	
		Options options = new Options();
		options.addOption("f", "file", true, "YAML jobs input file");
		options.addOption("h", "help", false, "Print the help");

		CommandLineParser parser = new PosixParser();
		CommandLine cmd;
		try {
			cmd = parser.parse(options, args);
		} catch(ParseException e) {
			help("Impossible to parse arguments");
			throw e;
		}

		if (cmd.hasOption(("help"))) {
			help();
			System.exit(0);
		}
		
		if (!cmd.hasOption("file")) {
			help("you have to specify the input file");
			System.exit(-1);
		} else {
			inputFile = cmd.getOptionValue("file");
		}
	}

	private static void help() {
		help(null);
	}

	private static void help(String msg) {
		final String help = "Help ";
		System.out.println(msg);
		System.out.println();
		System.out.println(help);
		
	}

}
