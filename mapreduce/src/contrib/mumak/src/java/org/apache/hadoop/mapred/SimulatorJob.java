package org.apache.hadoop.mapred;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.AbstractContinuousDistribution;
import org.apache.commons.math.distribution.AbstractIntegerDistribution;
import org.apache.commons.math.distribution.Distribution;
import org.apache.commons.math.distribution.NormalDistributionImpl;
import org.apache.commons.math.random.JDKRandomGenerator;
import org.apache.commons.math.random.RandomGenerator;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.JobACL;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.TaskType;
import org.apache.hadoop.mapreduce.jobhistory.HistoryEvent;
import org.apache.hadoop.mapreduce.jobhistory.JobFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.JobInitedEvent;
import org.apache.hadoop.mapreduce.jobhistory.JobSubmittedEvent;
import org.apache.hadoop.mapreduce.jobhistory.MapAttemptFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.ReduceAttemptFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskAttemptFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskAttemptStartedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskFinishedEvent;
import org.apache.hadoop.mapreduce.jobhistory.TaskStartedEvent;
import org.apache.hadoop.security.authorize.AccessControlList;
import org.apache.hadoop.tools.rumen.JobBuilder;
import org.apache.hadoop.tools.rumen.LoggedJob;
import org.apache.hadoop.tools.rumen.LoggedTask;
import org.apache.hadoop.tools.rumen.LoggedTaskAttempt;
import org.apache.hadoop.tools.rumen.Pre21JobHistoryConstants.Values;

public class SimulatorJob extends LoggedJob {

	final static Map<String, Class<? extends Distribution>> distributions = new HashMap<String, Class<? extends Distribution>>();
	static {
		distributions.put("NormalDistributionImpl",
				NormalDistributionImpl.class);
	}

	private String mapDurationDistribution;
	private Map<String, String> mapDistributionParameters;
	private String reduceDurationDistribution;
	private Map<String, String> reduceDistributionParameters;
	private String jobTrackerName;
	private int jid_number;
	private JobID jid;
	
	private boolean template = false;

	public SimulatorJob() {
	}

	public String getMapDurationDistribution() {
		return mapDurationDistribution;
	}

	public void setMapDurationDistribution(String mapDurationDistribution) {
		// distributions.
		this.mapDurationDistribution = mapDurationDistribution;
	}

	public Map<String, String> getMapDistributionParameters() {
		return mapDistributionParameters;
	}

	public void setMapDistributionParameters(
			Map<String, String> mapDistributionParameters) {
		this.mapDistributionParameters = mapDistributionParameters;
	}

	public String getReduceDurationDistribution() {
		return reduceDurationDistribution;
	}

	public void setReduceDurationDistribution(String reduceDurationDistribution) {
		this.reduceDurationDistribution = reduceDurationDistribution;
	}

	public Map<String, String> getReduceDistributionParameters() {
		return reduceDistributionParameters;
	}

	public void setReduceDistributionParameters(
			Map<String, String> reduceDistributionParameters) {
		this.reduceDistributionParameters = reduceDistributionParameters;
	}
	
	public boolean isTemplate() {
		return template;
	}

	public void setTemplate(boolean template) {
		this.template = template;
	}

	public String toString() {
		return "SimpleJob: (" + (template?"TEMPLATE ":"") + "totalMaps = " + getTotalMaps() + ", "
				+ "totalReduces = " + getTotalReduces() + ","
				+ "mapDistribution = " + mapDurationDistribution + ", "
				+ "reduceDistribution " + reduceDurationDistribution + ", "
				+ "mapDistributionParams = " + mapDistributionParameters + ","
				+ "reduceDistributionParams " + reduceDistributionParameters
				+ ")";

	}

	public LoggedJob build(int counter) throws JobBuilderException,
			MathException {
		System.out.println("Building job " + counter);
		RandomGenerator generator = new JDKRandomGenerator();
		long seed = 0l;
		generator.setSeed(seed);
		jobTrackerName = "fakejt";
		jid = getJobID(jobTrackerName, counter);
		jid_number = counter;
		this.setJobID(jid.toString());
		if(getJobName() == null)
			this.setJobName("job" + counter);
		
		if (getUser()==null)
			this.setUser("userhadoop");
		

		JobBuilder jb = new JobBuilder(jid.toString());

		jb.process(generateJobSubmissionEvent());
		jb.process(generateJobStartEvent());
		generateRandomMapTasks(jb, generator, jid);
		generateRandomReduceTasks(jb, generator, jid);
		jb.process(generateJobFinishEvent());

		return jb.build();
	}

	private JobID getJobID(String jt, int counter) {
		return new JobID(jt, counter);
	}

	private HistoryEvent generateJobSubmissionEvent() {
		// JobSubmittedEvent ev = new JobSubmittedEvent(jid, getJobName(),
		// getUser(), getSubmitTime(), jobConfPath(), getJobACLs());
		Map<JobACL, AccessControlList> jobACLs = new HashMap<JobACL, AccessControlList>();
		return new JobSubmittedEvent(jid, getJobName(), getUser(),
				getSubmitTime(), "", jobACLs);
	}

	private HistoryEvent generateJobStartEvent() {
		String status = JobStatus.State.SUCCEEDED.toString();
		return new JobInitedEvent(jid, getLaunchTime(), getTotalMaps(),
				getTotalReduces(), status);

	}

	private HistoryEvent generateJobFinishEvent() {
		int finishedMaps = getTotalMaps();
		int finishedReduces = getTotalReduces();
		int failedMaps = 0;
		int failedReduces = 0;
		Counters counters = new Counters();

		JobFinishedEvent ev = new JobFinishedEvent(jid, getFinishTime(),
				finishedMaps, finishedReduces, failedMaps, failedReduces,
				counters, counters, counters);
		return ev;
	}

	// private void generateReduceTasks(JobBuilder jb) {
	// // TODO Auto-generated method stub
	// ReduceAttemptFinishedEvent ev;
	// if (getReduceTasks().isEmpty() && getTotalReduces() > 0) {
	// Distribution d = initDistribution(mapDurationDistribution,
	// mapDistributionParameters);
	// generateRandomTasks(totalReduces, generator, d, reduceTaskList);
	// }
	// }

	private void generateRandomMapTasks(JobBuilder jb,
			RandomGenerator generator, JobID jid) throws JobBuilderException,
			MathException {
		if (getMapTasks().isEmpty() && getTotalMaps() > 0) {
			Distribution d = initDistribution(mapDurationDistribution,
					mapDistributionParameters);
			// generateRandomTasks(totalMaps, generator, d,
			// mapTaskList);v---------------------------------------------
			generateRandomTasks(generator, d, TaskType.MAP, jb, jid);
		}
	}

	private void generateRandomReduceTasks(JobBuilder jb,
			RandomGenerator generator, JobID jid) throws JobBuilderException,
			MathException {
		if (getReduceTasks().isEmpty() && getTotalReduces() > 0) {
			Distribution d = initDistribution(reduceDurationDistribution,
					reduceDistributionParameters);
			generateRandomTasks(generator, d, TaskType.REDUCE, jb, jid);
		}
	}

	private void generateRandomTasks(RandomGenerator generator, Distribution d,
			TaskType type, JobBuilder jb, JobID jid) throws MathException,
			JobBuilderException {
		int totalTasks = (type == TaskType.MAP) ? getTotalMaps()
				: getTotalReduces();

		for (int i = 0; i < totalTasks; i++) {
			double taskLen;
			if (d instanceof AbstractContinuousDistribution) {
				AbstractContinuousDistribution acd = (AbstractContinuousDistribution) d;
				taskLen = acd.inverseCumulativeProbability(generator
						.nextDouble());
			} else if (d instanceof AbstractIntegerDistribution) {
				AbstractIntegerDistribution aid = (AbstractIntegerDistribution) d;
				taskLen = aid.inverseCumulativeProbability(generator
						.nextDouble());
			} else {
				throw new JobBuilderException("The specified distribution(" + d
						+ ") cannot be inverted ");
			}

			TaskID tid = new TaskID(jid, type, i);
			TaskAttemptID attemptId = new TaskAttemptID(jobTrackerName,
					jid_number, type, i, 0);

			String splitLocationsString = "/hostA/rack";
			String trackerName = "/hostA/rack";
			int httpPort = 1123;
			String status = "SUCCESS"; // TaskStatus.State.SUCCEEDED.toString();
			Counters counters = new Counters();
			long finishTime = (long) (this.getSubmitTime() + taskLen);

			jb.process(new TaskStartedEvent(tid, this.getSubmitTime(), type,
					splitLocationsString));
			jb.process(new TaskAttemptStartedEvent(attemptId, type, this
					.getSubmitTime(), trackerName, httpPort));
			jb.process(new TaskFinishedEvent(tid, finishTime, type, status,
					counters));
			if (type == TaskType.MAP)
				jb.process(new MapAttemptFinishedEvent(attemptId, type, status,
						finishTime, finishTime, trackerName, status, counters));
			else if (type == TaskType.REDUCE) {
				long shuffleFinishTime = (long) (this.getSubmitTime() + taskLen/3);
				long sortFinishTime = (long) (this.getSubmitTime() + 2*taskLen/3);
				jb.process(new ReduceAttemptFinishedEvent(attemptId, type,
						status, shuffleFinishTime, sortFinishTime,
						finishTime, trackerName, status, counters));
			}
			jb.process(new TaskAttemptFinishedEvent(attemptId, type, status,
					finishTime, trackerName, status, counters));
		}

	}

	private Distribution initDistribution(String distributionName,
			Map<String, String> distributionParams) throws JobBuilderException {

		Class c = distributions.get(distributionName);
		Distribution d;
		try {
			d = (Distribution) c.newInstance();
			Map<String, Object> properties = BeanUtils.describe(d);
			for (Map.Entry<String, String> entry : distributionParams
					.entrySet()) {
				String methodName = entry.getKey();
				String value = entry.getValue();
				BeanUtils.setProperty(d, methodName, value);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution + "
					+ distributionName + " -- " + e.toString());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution + "
					+ distributionName + " -- " + e.toString());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution + "
					+ distributionName + " -- " + e.toString());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new JobBuilderException("Impossible to init distribution + "
					+ distributionName + " -- " + e.toString());
		}
		return d;
	}
}
