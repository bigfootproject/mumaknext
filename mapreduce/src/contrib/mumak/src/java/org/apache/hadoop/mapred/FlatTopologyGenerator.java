package org.apache.hadoop.mapred;

import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.tools.rumen.LoggedNetworkTopology;
import org.apache.hadoop.tools.rumen.MachineNode;
import org.apache.hadoop.tools.rumen.ParsedHost;

public class FlatTopologyGenerator {
	private LoggedNetworkTopology topology;

	public FlatTopologyGenerator(int machineNumber, MachineNode defaultNode) {
		Set<ParsedHost> hosts = new HashSet<ParsedHost>();
		for (int i=0; i<machineNumber; i++) {
			ParsedHost p = new ParsedHost("<root>", "machine_" + i);
			hosts.add(p);
		}
		topology = new LoggedNetworkTopology(hosts);
	}

	/**
	 * Get the {@link LoggedNetworkTopology} object.
	 * 
	 * @return The {@link LoggedNetworkTopology} object parsed from the input.
	 */
	public LoggedNetworkTopology get() {
		return topology;
	}

}
