package org.apache.hadoop.mapred;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.tools.rumen.ClusterTopologyReader;
import org.apache.hadoop.tools.rumen.JobStoryProducer;
import org.apache.hadoop.tools.rumen.LoggedNetworkTopology;
import org.apache.hadoop.tools.rumen.MachineNode;
import org.apache.hadoop.tools.rumen.RandomSeedGenerator;
import org.apache.hadoop.tools.rumen.ZombieCluster;
import org.apache.hadoop.util.ToolRunner;

@SuppressWarnings("deprecation")
public class SimulatorEngine2 extends SimulatorEngine {

	public enum ClusterInitType {
		FLATTOPOLOGY, LEGACYMUMAK;
	}

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		System.out.println("Mumak Next");
		int res = ToolRunner.run(new Configuration(), new SimulatorEngine2(),
				args);
		System.exit(res);
	}

	ClusterInitType clusterInitType = ClusterInitType.LEGACYMUMAK;
	int machineNumber = -1;

	@Override
	void parseParameters(String[] args) {
		try {
			Options options = new Options();
			options.addOption("p", "topology", true, "The topology");
			options.addOption("f", "topologyFile", true, "Json topology file");
			options.addOption("M", "machineNumber", true,
					"The number of machines in the cluster (static topology)");
			options.addOption("m", "mapSlots", true,
					"Number of mapSlots  (static topology)");
			options.addOption("r", "reduceSlots", true,
					"Number of reduce slots  (static topology)");
			options.addOption("t", "traceFile", true, "Json trace file");
			options.addOption("h", "help", false, "Print the help");

			CommandLineParser parser = new PosixParser();
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption(("help"))) {
				System.out.println("Help");
				System.exit(0);
			}
			
			//OTHER OPTIONS
			
			if (cmd.hasOption("mapSlots")) {
				getConf().setInt("mapred.tasktracker.map.tasks.maximum", Integer.parseInt(cmd.getOptionValue("mapSlots")));
			}
			if (cmd.hasOption("reduceSlots")) {
				getConf().setInt("mapred.tasktracker.reduce.tasks.maximum", Integer.parseInt(cmd.getOptionValue("reduceSlots")));
			}
			
			// TOPOLOGY
			if (cmd.hasOption("topology")
					&& cmd.getOptionValue("topology").equals("STATIC")) {
				clusterInitType = ClusterInitType.FLATTOPOLOGY;
				machineNumber = Integer.parseInt(cmd
						.getOptionValue("machineNumber"));
			} else if (cmd.hasOption("topologyFile")) {
				topologyFile = cmd.getOptionValue("topologyFile");
				System.out.println("Added topologyFile" + topologyFile);
			} else {
				throw new IllegalArgumentException(
						"Wrong Topology");
			}
			
			

			// TraceFile
			if (cmd.hasOption("traceFile")) {
				traceFile = cmd.getOptionValue("traceFile");
			}
		} catch (ParseException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(
					"Parse Exception Usage: java ... SimulatorEngine trace.json topology.json");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.hadoop.mapred.SimulatorEngine#createCluster(org.apache.hadoop
	 * .mapred.JobConf)
	 */
	@Override
	protected ZombieCluster createCluster(JobConf jobConf) throws IOException {
		if (clusterInitType == ClusterInitType.LEGACYMUMAK) {
			return super.createCluster(jobConf);
		} else if(clusterInitType == ClusterInitType.FLATTOPOLOGY) {
		    int maxMaps = getConf().getInt(
		        "mapred.tasktracker.map.tasks.maximum",
		        SimulatorTaskTracker.DEFAULT_MAP_SLOTS);
		    int maxReduces = getConf().getInt(
		        "mapred.tasktracker.reduce.tasks.maximum",	    
		      SimulatorTaskTracker.DEFAULT_REDUCE_SLOTS);

		    MachineNode defaultNode = new MachineNode.Builder("default", 2)
		        .setMapSlots(maxMaps).setReduceSlots(maxReduces).build();
		            
		    LoggedNetworkTopology topology = new FlatTopologyGenerator(machineNumber, defaultNode).get();
		    		
		    // Setting the static mapping before removing numeric IP hosts.
		    setStaticMapping(topology);
		    if (getConf().getBoolean("mumak.topology.filter-numeric-ips", true)) {
		      removeIpHosts(topology);
		    }
		    ZombieCluster cluster = new ZombieCluster(topology, defaultNode);
			return cluster;
		}
		else
			throw new IOException("Invalid Cluster Type");
	}

	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapred.SimulatorEngine#getJobList(org.apache.hadoop.mapred.JobConf, org.apache.hadoop.tools.rumen.ZombieCluster, long)
	 */
	@Override
	JobStoryProducer getJobList(JobConf jobConf, ZombieCluster cluster,
			long firstJobStartTime) throws IOException {
		return super.getJobList(jobConf, cluster, firstJobStartTime);
	} 
}
